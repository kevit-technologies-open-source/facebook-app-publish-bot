# ![](https://lh6.googleusercontent.com/Af5wamq6m8gHzAvQOgrNbgNaA6XDwGCEkVxyYEu1CGEiWMbwhyZLX4xL4ViXybX6-ZoMCx02qyRK04YoKpk58Bp-TvVGzsK82Qn8WNjT1TWVKKgXpz9ywNTZNnh7YYfGDOWMumc4)
## Facebook App reviewed automatically
  - We know that for submitting app review we need to make many Facebook graph API calls to make test users, test Page and many more. These API calls are quite difficult for non-tech users and complex & lengthy for technical users too. So to simplify this process we made a CLI [Command Line Interface] for this process. 
    
   - The user needs to enter some Input and the process will automatically perform and your Test user, UserName and password will be generated automatically. Test Page will also be created and also it will be linked with the Facebook app.` 

- So let’s get started.

- Go to https://developers.facebook.com/apps select your app and copy App Id.

![alt text](https://dev.r3stemcell.botchef.io/assets/images/appId.png)


- Next, you will need “App Secret”

![alt text](https://dev.r3stemcell.botchef.io/assets/images/scecret.png)

## Installation
- This is a Node.js module available through the npm registry.

- Before installing, download and install Node.js. Node.js 0.10 or higher is required.

- Installation is done using the npm install command:
```bash
npm install @kevit/facebook-app-publish-bot -g
```
- Next step is
```bash
facebook-app-publish-bot init
```
## Usage
- Follow the video For next steps


[![CLI](http://blog.openwebsolutions.in/wp-content/uploads/2018/01/banner-of-cli-1024x497.jpg)](https://www.youtube.com/watch?v=KOxbO0EI4fMA "CLI")
