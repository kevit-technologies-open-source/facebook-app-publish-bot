let inquirer = require('inquirer');
const homedir = require('os').homedir();
const fs = require('fs');
const path = require('path');
var Table = require('cli-table');
const rp=require('request-promise');
var Spinner = require('cli-spinner').Spinner;

var spinner = new Spinner('processing.. %s');
spinner.setSpinnerString('|/-\\');

appToken=()=>{
    let questions = [
        {
            type: 'input',
            name: 'clientId',
            message: "clientId: ",
            validate:ans=>{
                return !!(ans.length)//to make condition

            }
        }, {
            type: 'password',
            name: 'clientSecret',
            message: "clientSecret: ",
            mask: '*',
            validate:ans=>{
                return !!(ans.length)
            }
        }
    ];
    inquirer.prompt(questions).then(ans => {
        spinner.start();
        let option={
           method:'GET',
           uri:'https://graph.facebook.com/oauth/access_token?client_id='+ans.clientId+'&client_secret='+ans.clientSecret+'&grant_type=client_credentials',
           json:true
       }
       rp(option).then(data=>{
           let obj = {
               clientId: ans.clientId.trim(),
               clientSecret: ans.clientSecret.trim(),
               access_token:data.access_token
           }
           console.log(homedir)
           if (fs.existsSync(`${homedir}/.FB/credentials.json`)) {
               fs.writeFile(`${homedir}/.FB/credentials.json`, JSON.stringify(obj), function (err) {
                   if (err) {
                       spinner.stop();
                       return console.log(err);
                   }
                   spinner.stop();
                   console.log("Success! You're logged in.");
                   TestUser()
               });
           }else {
               fs.mkdirSync(path.join(homedir, '.FB'));
               fs.writeFile(`${homedir}/.FB/credentials.json`, JSON.stringify(obj), function (err) {
                   if (err) {
                       spinner.stop();
                       return console.log(err);
                   }
                   console.log("Success! Get App Access Token.");
                   spinner.stop()
                   TestUser()
               });
           }

       }).catch(e=>{
           spinner.stop()
           console.error(e.message)
           appToken()

       })

    }).catch(e=>{
        console.error(e.message)
        spinner.stop()
        appToken()
    })


};
TestUser=()=>{
    let questions = [
        {
            type: 'checkbox',
            name: 'Permission',
            message: "Permission: ",
            choices:[{name:"All",checked:true},{name:'Required(manage_pages,pages_messaging,publish_pages)',checked:false}],
            validate:ans=>{
                return !!(ans.length)

            }
        }
    ];

    inquirer.prompt(questions).then(ans => {
        let uri;
        let contents = fs.readFileSync(`${homedir}/.FB/credentials.json`, 'utf8');
        let access_token=JSON.parse(contents).access_token
        if(ans.Permission.length===2){
          uri='https://graph.facebook.com/v4.0/460954188015051/accounts/test-users?installed=true&permissions=publish_pages,pages_messaging,manage_pages,publish_pages&name=&access_token='+access_token
        }else {
            uri='https://graph.facebook.com/v4.0/460954188015051/accounts/test-users?installed=true&permissions=publish_pages,pages_messaging,manage_pages,publish_pages&name=&access_token='+access_token

        }
        spinner.start()
        let TestUseroption={
            method: 'POST',
            uri:uri,
            json: true
        };
        rp(TestUseroption).then(TestUserData=>{
            let json=JSON.parse(contents);
            json.TestUserData=TestUserData
            fs.writeFile(`${homedir}/.FB/credentials.json`, JSON.stringify(json), function (err) {
                if (err) {
                    return console.error(err);
                }
                console.log("Test User Created");
                spinner.stop()
                TestPage()
            });
        }).catch(Error=>{
            console.log(Error.message);
            spinner.stop()
            TestUser()
        })
    }).catch(e=>{
        console.error(e.message)
        spinner.stop()
        TestUser()


    })



};
TestPage=()=>{
    let questions = [
        {
            type: 'input',
            name: 'name',
            message: "Enter Page Name: ",
            validate:ans=>{
                return !!(ans.length)
              
            }
        },
        {
            type: 'input',
            name: 'about',
            message: "Enter About your Page",
            validate:ans=>{
                return !!(ans.length)

            }
        },{
            type: 'input',
            name: 'picture',
            message: "Enter picture url of your Page ",
            validate:ans=>{
                return !!(ans.length)
            }
        },
        {
            type: 'input',
            name: 'cover_photo',
            message: "Enter cover_photo Url of your Page ",
            validate:ans=>{
                return !!(ans.length)
            }
        }
    ];
inquirer.prompt(questions).then(data=>{
    spinner.start()
    let contents = fs.readFileSync(`${homedir}/.FB/credentials.json`, 'utf8');
    let access_token=JSON.parse(contents).TestUserData.access_token
    let Test_User_id=JSON.parse(contents).TestUserData.id;
    let TestPageoption={
        method:'POST',
        json:true,
        uri:'https://graph.facebook.com/v3.3/'+Test_User_id+'/accounts?access_token='+access_token,
        body:{
            name:data.name.trim(),
            category_enum:'TOPIC_JUST_FOR_FUN',
            about:data.about,
            picture:data.picture,
            cover_photo:{
                url:data.cover_photo
            }
        }
    }
    rp(TestPageoption).then(TestPageData=>{
        let json=JSON.parse(contents);
        json.TestpageId=TestPageData.id;
        fs.writeFile(`${homedir}/.FB/credentials.json`, JSON.stringify(json), function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("Test Page Created");
            spinner.stop()
            pageAccesToken()
        });
    }).catch(e=>{
        console.error("Error in create page",e.message)
        spinner.stop()
        TestPage()


    })
}).catch(e=>{
    console.error(e.message);
    spinner.stop()
    TestPage()

})
}
pageAccesToken=()=>{
    spinner.start()
    let contents = fs.readFileSync(`${homedir}/.FB/credentials.json`, 'utf8');
    let access_token=JSON.parse(contents).TestUserData.access_token
    let page_id=JSON.parse(contents).TestpageId
    let pageaccestoken={
        method:'GET',
        json:true,
        uri:'https://graph.facebook.com/me/accounts',
        qs: { access_token:  access_token}
    }
    rp(pageaccestoken).then(data=>{
        data.data.forEach(d=>{
            if(d.id===page_id){
                let json=JSON.parse(contents)
                json.pageAccesToke=d.access_token
                fs.writeFile(`${homedir}/.FB/credentials.json`, JSON.stringify(json), function (err) {
                    if (err) {
                        return console.log(err);
                    }
                    console.log(" Page Access Token Created");
                    spinner.stop()
                    AppSubcribe()
                });

            }
        })
    }).catch(e=>{
        console.error(e)
        spinner.stop()
        pageAccesToken()
    })
}
AppSubcribe=()=>{
    spinner.start()
    let contents = fs.readFileSync(`${homedir}/.FB/credentials.json`, 'utf8');
    let access_token=JSON.parse(contents).pageAccesToke;
    let page_id=JSON.parse(contents).TestpageId
    let appsubcribe={
        method:'POST',
        json:true,
        uri:'https://graph.facebook.com/v4.0/'+page_id+'/subscribed_apps?access_token='+access_token+'&subscribed_fields=messages'
    }
    rp(appsubcribe).then(data=>{
        spinner.stop()
        UserData();
    }).catch(e=>{
        console.error(e.message)
        spinner.stop()
        AppSubcribe()
    })
}

UserData=()=>{
    let contents = fs.readFileSync(`${homedir}/.FB/credentials.json`, 'utf8');
    var email=JSON.parse(contents).TestUserData.email;
    let password=JSON.parse(contents).TestUserData.password;
    let page_id=JSON.parse(contents).TestpageId

    var table = new Table();
    table.push(
             { 'User_Email': email || 'Email Not Found'}
        , { 'Password ': password || 'password Not found'},{
                 Page_url:'https://www.facebook.com/'+page_id || "Page Not create"
        }
    );

    console.log(table.toString());
};
module.exports = {appToken,TestUser,TestPage,AppSubcribe,pageAccesToken,UserData};
