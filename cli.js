#!/usr/bin/env node

let program = require('commander');
let Fb=require('./commands/Fb')
program
    .version("0.0.1")
    .description("Start Making Bot review!")
    .arguments("<command>")
    .command("App Review <operation>", "TestUser/TestPage/AppLink");
program
    .command("init")
    .alias("i")
    .description("Initialize App Review")
    .action((cmd, options) => {
        Fb.appToken()
    });
program
    .command("UserData")
    .alias("i")
    .description("UserData Display ")
    .action((cmd, options) => {
        Fb.UserData()
    });

program.parse(process.argv);
